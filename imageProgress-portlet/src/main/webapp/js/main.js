//JXG.Options.renderer = 'canvas';
var JSXMaster = {
	fakePoint: undefined, //this point is the position of mouse on the screen (always)
	fakeText: undefined,
	board: undefined,
	scale: 100,
	mapWidth: undefined,
	mapHeight: undefined,
	init : function(){
		JSXMaster.mapWidth = window.innerWidth/JSXMaster.scale;
		JSXMaster.mapHeight = window.innerHeight/JSXMaster.scale;
		var w = JSXMaster.mapWidth, h = JSXMaster.mapHeight;
		//var dim = (w > h) ? h : w;
		//dim = dim/100;
		
		JSXMaster.board = JXG.JSXGraph.initBoard('jxgbox', {boundingbox: [-w/2,h/2,w/2,-h/2], 
			axis: false, 
			grid: true,
			showCopyright: false, 
			showNavigation: true,
			zoom: {
				wheel: true
			}
		});
		JSXMaster.fakePoint = JSXMaster.board.create('point', [0, 0], {name: '', size:3});	
		JSXMaster.fakeText = JSXMaster.board.create('text', [0, 0, ''], {display: 'html'});
		//JSXMaster.fakeText.hideElement();
		//var tape = JSXMaster.board.create('tapemeasure', [[0, 0], [1, 1]], {name:'dist'});
		//alert(JSON.stringify(tape.getLabelText));
		//JSXMaster.board.addMouseEventHandlers();
		//alert(JSXMaster.board.);
		//alert(JSXMaster.board.mouseWheelListener);
		//JSXMaster.board.on("mousewheel", JSXMaster.draggingBoard);
		//JSXMaster.board.mouseWheelListener = JSXMaster.mousewheel;
		JSXMaster.board.gestureStartListener = JSXMaster.mousewheel;

		//JSXMaster.fakePoint.setLabelText("");
		JSXMaster.board.addEventHandlers();
		//alert(JSXMaster.board.mouseWheelListener);
	},
	getMouseCoords : function(e, i) {
		var cPos = JSXMaster.board.getCoordsTopLeftCorner(e, i),
			absPos = JXG.getPosition(e, i),
			dx = absPos[0]-cPos[0],
			dy = absPos[1]-cPos[1];
		//console.log("update mouse coord");
		var res = new JXG.Coords(JXG.COORDS_BY_SCREEN, [dx, dy], JSXMaster.board);
		JSXMaster.currentMouseCoords = res.usrCoords.slice(1);
		return res;
	},
	currentMouseCoords: undefined,
	hidepoint: function(){
		if(JSXMaster.fakePoint !== undefined)
			JSXMaster.fakePoint.hideElement();
	},
	mousewheel: function(evt){
		//var pos;
		//pos = this.getMousePosition(evt);
		//var x = JSXMaster.board.origin.scrCoords[1],
		//	y = JSXMaster.board.origin.scrCoords[2];
			
		//JSXMaster.board.moveOrigin(x, y - y * 0.1);
		console.log("wheeling");
		//JSXMaster.board.mouseMoveListener(evt);
	}
};
JSXMaster.init();

var Polygon = {
	currLine: undefined,
	prefix: "poly",
	counter: 0,
	
	/**
	@field: polygons
	@type: dictionary of polygons drawn on the screen.
	@structure: key: polygonId, value: an array of vertices belonging to @polygonId 
	@aim: many things. 
	@example: {poly1: [v1, v2,...,vn], poly2: [], ....}
	*/
	polygons: {}, 
	
	/**
	@field: lines
	@structure: key: polygonId, value: an array of edges belonging to @polygonId
	@type: dictionary of edges of a polygon.
	@aim: the set of lines belonging to the current polygon. This set is used only for binding events.
	*/
	lines: {}, 
	
	/**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: polygonID. 
	@aim: This dictionary is used for lookup a polygon that a vertex belongs to.
	*/
	mappingPoints: {}, 
	
	down : function(e) {
		//handler when we press the mouse on the screen.
		var canCreate = true, 
			coords, 
			polygonID, 
			points;
 
		//if (e[JXG.touchProperty]) {
			// index of the finger that is used to extract the coordinates
			//i = 0;
		//}
		coords = JSXMaster.getMouseCoords(e, 0);
		polygonID = Polygon.prefix + "_" + Polygon.counter;
		//alert(polygonID);
		if(Polygon.ableToCreate(polygonID, e)){
	
			console.log("Can create");
			var first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], {name:''});
			
			if(Polygon.currLine !== undefined){
				//complete the previous line. 
				Polygon.currLine.point2 = first;
			}
			Polygon.currLine = JSXMaster.board.create('line', [first, JSXMaster.fakePoint], 
										{straightFirst:false, straightLast:false, strokeWidth:2});
										
			
			
			//set of lines of current polygon.
			Polygon.lines[polygonID] = Polygon.lines[polygonID] || [] ;
			Polygon.lines[polygonID].push(Polygon.currLine);
			//Polygon.lines[polygonID] = list;
			
			//add the new vertex to the polygon @polygonID
			Polygon.polygons[polygonID] = Polygon.polygons[polygonID] || [] ;
			Polygon.polygons[polygonID].push(first);
			
			//this vertex belongs to @polygonID
			Polygon.mappingPoints[first.id] = polygonID;
			
		}
	},
	initHoverBinding: function(polyId){
		var lines = Polygon.lines[polyId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('over', Polygon.overLine);
			lines[i].on('out', Polygon.outLine);
		}
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		JSXMaster.fakePoint.hideElement();
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//self.withLabel = false;
		JSXMaster.fakePoint.showElement();
		JSXMaster.fakeText.hideElement();
	},
	/**
		Check if we can create new point and new line.
		@polygonID
		@event e: down event object
	*/
	ableToCreate : function(polygonID, e){
		var coords, points, canCreate = true;
		
		coords = JSXMaster.getMouseCoords(e, 0);
		points = Polygon.polygons[polygonID] || [];
		for(var i=0; i<points.length; i++){
			
			var point = points[i];
			
			//The condition that inform us that a cycle (a polygon) is created.
			if(point.id !== JSXMaster.fakePoint.id && point.hasPoint(coords.scrCoords[1], coords.scrCoords[2]))
			{
				Polygon.completePolygon(point);
				Polygon.initHoverBinding(polygonID);
				Polygon.initDraggingListener(polygonID);
				canCreate = false;
				break;
			}
		}
		return canCreate;
	},
	/**a cycle of points was created. 
	we need to complete the current polygon and reset data for the new one*/
	completePolygon: function(lastPoint){
		Polygon.currLine.point2 = lastPoint;
		Polygon.currLine = undefined;
		//Polygon.points = {};
		//Polygon.lines = [];
		Polygon.counter++; //increase the id of polygon
	},
	/** Binding dragging event for each edge */
	initDraggingListener: function(polygonID){
		var lines = Polygon.lines[polygonID] || [];
				
		for(var j = 0; j<lines.length; j++){
			var line = lines[j];
			line.on('down', function(e){
				JSXMaster.getMouseCoords(e, 0);
			});
			line.on('drag', Polygon.dragging);
		}
	},
	dragging: function(e){
		//this: the line that we drag
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id],
			points = Polygon.polygons[polygonID] || [];
		//alert("polygonID: " + polygonID);
		//alert(JSON.stringify(points));
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		//console.log("begin point: " + JSON.stringify(begin));
		//console.log("end point: " + JSON.stringify(end));
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = this.point1, 
			second = this.point2;
		//first.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
		
		//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
		for(var k=0; k<points.length; k++){
			var nextPts = points[k];
			if(nextPts.id != first.id && nextPts.id != second.id){
				nextPts.setPosition(JXG.COORDS_BY_USER, [nextPts.X() + vector.x, nextPts.Y() + vector.y]);
				nextPts.prepareUpdate().update().updateRenderer();
			} 
		}
	},
	binding: function(){
		JSXMaster.board.on('down', Polygon.down);
		JSXMaster.board.on('mousemove', Polygon.mousemove);
		JSXMaster.board.on('mouseup', Polygon.mouseup);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Polygon.down);
		JSXMaster.board.off('mousemove', Polygon.mousemove);
		JSXMaster.board.off('mouseup', Polygon.mouseup);
	},
	mousemove: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Polygon.currLine !== undefined){
			Polygon.currLine.prepareUpdate().update().updateRenderer();
		}
		//http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html
		
		//Polygon.currLine.point2.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		
		
	},
	mouseup: function(e){
	}
};
var Circle = {
	circle: undefined,
	circles: {},
	down: function(e){
		var shouldCreate = true, i, coords, el, center;
		coords = JSXMaster.getMouseCoords(e, 0);
		
		if (shouldCreate){
			center = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], {name:''});
		}
		
		//console.log("create circle");
		//var center = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]]);
		if(Circle.circle !== undefined){
			Circle.circle.point2 = center;
			Circle.circle = undefined;
			return;
		}
		Circle.circle = JSXMaster.board.createElement('circle',[center, JSXMaster.fakePoint], 
			{   strokeColor:'#00ff00',strokeWidth:2});
		Circle.circles[center.id] = Circle.circle;
		Circle.circle.on('down', function(e){
			JSXMaster.getMouseCoords(e, 0); 
		});		
		Circle.circle.on('drag', function(e){
			var self = this;
			var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
			JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
			var end = JSXMaster.currentMouseCoords;
			
			//console.log("begin point: " + JSON.stringify(begin));
			//console.log("end point: " + JSON.stringify(end));
			var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
			var first = this.center, 
				second = this.point2;
			//first.prepareUpdate().update().updateRenderer();
			second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
			second.prepareUpdate().update().updateRenderer();
			
			//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
			
		});
		
		Circle.circle.center.on('down', function(e){
			JSXMaster.getMouseCoords(e, 0); 
		});
		Circle.circle.center.on('drag', function(e){
			var self = this;
			var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
			JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
			var end = JSXMaster.currentMouseCoords;
			
			//console.log("begin point: " + JSON.stringify(begin));
			//console.log("end point: " + JSON.stringify(end));
			var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
			var main_circle = Circle.circles[self.id],
				second = main_circle.point2;
			//var second = this;
			//first.prepareUpdate().update().updateRenderer();
			second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
			second.prepareUpdate().update().updateRenderer();
			
			//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
			
		});
		
	},
	move: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		//JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Circle.circle !== undefined){
			Circle.circle.prepareUpdate().update().updateRenderer();
		}
		
		//getTextAnchor
		
	},
	up: function(e){
		//Circle.unbinding();
	},
	binding: function(){
		JSXMaster.board.on('down', Circle.down);
		JSXMaster.board.on('move', Circle.move);
		JSXMaster.board.on('up', Circle.up);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Circle.down);
		JSXMaster.board.off('move', Circle.move);
		JSXMaster.board.off('up', Circle.up);
	}
};
var Line = {
	line: undefined,
	currMousePosition : undefined,
	lines: {},
	down: function(e){
		var shouldCreate = true, i, coords, el, first;
		//coords = JSXMaster.getMouseCoords(e, 0);
		/*
		for (el in JSXMaster.board.objects) {
			
			if(el !== JSXMaster.fakePoint.id && JXG.isPoint(JSXMaster.board.objects[el]) && JSXMaster.board.objects[el].hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
				
				shouldCreate = false;
				first = JSXMaster.board.objects[el];
				break;
			}
		}
		*/
		//console.log("mouse down");
		if (shouldCreate){
			first = JSXMaster.board.create('point', JSXMaster.currentMouseCoords, {name:''});
		}
		
		if(Line.line !== undefined){
			Line.line.point2 = first;
			Line.line = undefined;
			return;
		}
		var options = { straightFirst:false, straightLast:false, strokeWidth:2};
		Line.line = JSXMaster.board.createElement('line',[first, JSXMaster.fakePoint], options);
			
		Line.line.setLabelText("Hello world");
		Line.line.on('over', Line.overLine);
		Line.line.on('out', Line.outLine);
		Line.lines[Line.line.id] = Line.line;
		Line.line.on('drag', function(e){
			//http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html#event:drag
			var self = this;
			var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
			JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
			var end = JSXMaster.currentMouseCoords;
			
			//console.log("begin point: " + JSON.stringify(begin));
			//console.log("end point: " + JSON.stringify(end));
			var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
			var first = this.point1, 
				second = this.point2;
			//first.prepareUpdate().update().updateRenderer();
			second.prepareUpdate().update().updateRenderer();
			
			//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
			second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

			
		});	
		Line.line.on('down', function(e){
			//http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html#event:down
			//this event will be fired and then 'drag' event on this line will fire. 
			JSXMaster.getMouseCoords(e, 0); 
		});
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		JSXMaster.fakePoint.hideElement();
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//self.withLabel = false;
		JSXMaster.fakePoint.showElement();
		JSXMaster.fakeText.hideElement();
	},
	move: function(e){
		//console.log('mouse move');
		JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, JSXMaster.currentMouseCoords);
		//JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Line.line !== undefined){
			Line.line.prepareUpdate().update().updateRenderer();
		}
		
		//getTextAnchor
		
	},
	up: function(e){
		//Circle.unbinding();
	},
	binding: function(){
		JSXMaster.board.on('down', Line.down);
		JSXMaster.board.on('move', Line.move);
		JSXMaster.board.on('up', Line.up);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Line.down);
		JSXMaster.board.off('move', Line.move);
		JSXMaster.board.off('up', Line.up);
	}
};
var Rectangle = {
	rectangle: undefined,
	//lines:[],
	//points:[],
	prefix: "rect",
	counter: 0,
	currRectId: undefined,
	/**
	@field: rects
	@type: dictionary of rectangles drawn on the screen.
	@structure: key: rectangleId, value: an array of vertices belonging to @rectangleId 
	@aim: many things. 
	@example: {rect1: [v1, v2,...,vn], rect2: [], ....}
	*/
	rects: {}, 
	
	/**
	@field: lines
	@structure: key: rectangleId, value: an array of edges belonging to @rectangleId
	@type: dictionary of edges of a rectangle.
	@aim: the set of lines belonging to the current rect. This set is used only for binding events.
	*/
	lines: {}, 
	
	/**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: rectangleID. 
	@aim: This dictionary is used for lookup a rectangle that a vertex belongs to.
	*/
	mappingPoints: {}, 
	down: function(e){
		var coords, first;
		
		coords = JSXMaster.getMouseCoords(e, 0);
		first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], {name:''});

		Rectangle.currRectId = Rectangle.prefix + "_" + Rectangle.counter;
		var rectId = Rectangle.currRectId;
		//alert(rectId);
		if(Rectangle.rectangle !== undefined){
			
			Rectangle.completeRect(rectId, first);
			Rectangle.initHoverBinding(rectId);
			Rectangle.initDraggingListener(rectId, e);
			return;
		}

		var p3 = {x: first.X(), y: first.Y()};
		var secondPoint = JSXMaster.board.create('point', [function(){return JSXMaster.fakePoint.X();}, p3.y], {name:''});
		
		var fourthPoint = JSXMaster.board.create('point', [p3.x, function(){return JSXMaster.fakePoint.Y();}], {name:''});
		
		Rectangle.rects[rectId] = Rectangle.rects[rectId] || [];
		Rectangle.rects[rectId].push(first);
		Rectangle.rects[rectId].push(secondPoint);
		Rectangle.rects[rectId].push(JSXMaster.fakePoint);
		Rectangle.rects[rectId].push(fourthPoint);
		
		var points = Rectangle.rects[rectId];
		var options = { straightFirst:false, straightLast:false, strokeWidth:2};
		var firstLine = JSXMaster.board.createElement('line',[points[0], points[1]], options);	
			
		var secondLine = JSXMaster.board.createElement('line',[points[1], points[2]], options);		
			
		var thirdLine = JSXMaster.board.createElement('line',[points[3], points[2]], options);	
			
		var fourthLine = JSXMaster.board.createElement('line',[points[3], points[0]], options);	
			
		Rectangle.lines[rectId] = Rectangle.lines[rectId] || [];
		Rectangle.lines[rectId].push(firstLine);
		Rectangle.lines[rectId].push(secondLine);
		Rectangle.lines[rectId].push(thirdLine);
		Rectangle.lines[rectId].push(fourthLine);

		Rectangle.rectangle = "created";

	},
	initHoverBinding: function(rectId){
		var lines = Rectangle.lines[rectId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('over', Rectangle.overLine);
			lines[i].on('out', Rectangle.outLine);
		}
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		JSXMaster.fakePoint.hideElement();
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//self.withLabel = false;
		JSXMaster.fakePoint.showElement();
		JSXMaster.fakeText.hideElement();
	},
	completeRect : function(rectId, lastpoint){
		
		//reset, very important
		Rectangle.rectangle = undefined;
			//Rectangle.secondPoint.addConstraint([x, y]);
		var points = Rectangle.rects[rectId] || [];
		points[1].free();
		points[3].free();
		points[2] = lastpoint;
		for(var i=0; i<points.length; i++){
			Rectangle.mappingPoints[points[i].id] = rectId;
		}
		var lines = Rectangle.lines[rectId] || [];
		lines[1].point2 = lastpoint;
		lines[2].point2 = lastpoint;
		Rectangle.counter++; //increase id of distinct rect
	},
	initDraggingListener: function(rectId, e){
		
		var down = function(e){
			JSXMaster.getMouseCoords(e, 0);
		}
		var lines = Rectangle.lines[rectId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('down', down);
		}
		lines[0].on('drag', Rectangle.dragging1);
		lines[1].on('drag', Rectangle.dragging2);
		lines[2].on('drag', Rectangle.dragging2);
		lines[3].on('drag', Rectangle.dragging1);
	},
	dragging2: function(e){
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = self.point1, second = self.point2;
		
		second.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

		//first.prepareUpdate().update().updateRenderer();
		var rectId = Rectangle.mappingPoints[first.id];
		var points = Rectangle.rects[rectId] || [];
		for(var i=0; i<points.length; i++){
			var p = points[i];
			if(p.id !== first.id && p.id !== second.id){
				p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
				p.prepareUpdate().update().updateRenderer();
			}
		}
	},
	dragging1 : function(e){
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = self.point1, second = self.point2;
		
		var rectId = Rectangle.mappingPoints[first.id];
		var points = Rectangle.rects[rectId] || [];
		for(var i=0; i<points.length; i++){
			var p = points[i];
			if(p.id !== first.id && p.id !== second.id){
				
				p.prepareUpdate().update().updateRenderer();
				p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
			}
		}
	},
	move: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		var rectId = Rectangle.currRectId;
		if(rectId === undefined){
			return;
		}
		
		var points = Rectangle.rects[rectId] || [];
		var lines = Rectangle.lines[rectId] || [];
		
		for(var i=0; i<points.length; i++){
			var point = points[i];
			if(point !== undefined)
				point.prepareUpdate().update().updateRenderer();
		}
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			line.prepareUpdate().update().updateRenderer();
		}
		
	},
	up: function(e){
		//Circle.unbinding();
	},
	binding: function(){
		JSXMaster.board.on('down', Rectangle.down);
		JSXMaster.board.on('move', Rectangle.move);
		JSXMaster.board.on('up', Rectangle.up);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Rectangle.down);
		JSXMaster.board.off('move', Rectangle.move);
		JSXMaster.board.off('up', Rectangle.up);
	}
};
//Polygon.init();
var GUI = function(){
	var self = this;
	var inputIdname = "inputId";
	//var tempF = ko.observable();
	self.InputId = ko.observable(inputIdname);
	self.MapJSON = ko.observable("");
	self.map = undefined;
	self.init = function(){
		//alert("no");
		$(document).ready(function(){
			$('div.toolbar_item').hover(function(){
				//alert('shit');
				$(this).addClass('toolbar_item_hover');
			}, function(){
				$(this).removeClass('toolbar_item_hover');
			});
		});
	};
	
	self.drawSquare = function(){
		//alert("draw square");
		self.reset();
		Rectangle.binding();
	}
	self.drawCircle = function(){
		//alert('drawCircle');
		self.reset();
		Circle.binding();
	}
	self.drawPolygon = function(){
		//alert('drawPolygon');
		self.reset();
		Polygon.binding();
	}
	self.drawLine = function(){
		self.reset();
		Line.binding();
		//alert('drawLine');
	}
	self.reset = function(){
		Polygon.unbinding();
		Circle.unbinding();
		Line.unbinding();
		Rectangle.unbinding();
		//JSXMaster.hidepoint();
	}
	self.removeAll = function(){
		JXG.JSXGraph.freeBoard(JSXMaster.board);
		JSXMaster.init();
	}
	self.saveFile = function(){
		//alert("save file");
		var map = {};
		var points = [];
		var lines = [];
		var circles = [];
		for (var el in JSXMaster.board.objects) {
			var obj = JSXMaster.board.objects[el];
			console.log(obj.getType());
			if(obj.getType() === 'point'){
				var point = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes()};
				points.push(point);
			}else if(obj.getType() === 'line'){
				var line = {id: obj.id, point1: obj.point1.id, point2: obj.point2.id, attrs: obj.getAttributes()};
				lines.push(line);
			}else if(obj.getType() === 'circle'){
				var circle = {id: obj.id, center: obj.center.id, point2: obj.point2.id, attrs: obj.getAttributes()};
				circles.push(circle);
			}
			//alert(s);
			//result += "," + s;
		}
		map["points"] = points;
		map["lines"] = lines;
		map["circles"] = circles;
		var result = JSON.stringify(map);
		var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "drawing.txt");
	}
	self.drawMap = function(map){
		self.removeAll();
		//alert(map.points.length);
		for(var i=0; i<map.points.length; i++){
			//alert("point la: " + JSON.stringify(map.points[i]));
			var point = map.points[i];
			var p = JSXMaster.board.create('point', [point.x, point.y], point.attrs);
			p.id = point.id;
		}
		
		
		for(var i=0; i<map.lines.length; i++){
			var line = map.lines[i];
			var p1 = JSXMaster.board.objects[line.point1];
			var p2 = JSXMaster.board.objects[line.point2];
			var l = JSXMaster.board.create('line', [p1, p2], line.attrs);
			l.id = line.id;
		}
		
		for(var i=0; i<map.circles.length; i++){
			var circle = map.circles[i];
			//alert("hell");
			var center = JSXMaster.board.objects[circle.center];
			var point2 = JSXMaster.board.objects[circle.point2];
			//alert("center la: " + center);
			//alert("point2 la: " + point2);
			var c = JSXMaster.board.create('circle', [center, point2], circle.attrs);
			c.id = circle.id;
		}
	}
	self.loadFile = function(){
		
		var elem = document.getElementById(self.InputId());
	   if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
	   }
		
		
		var fileInput = document.getElementById(self.InputId());
		var fileDisplayArea = "";

		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var textType = /text.*/;

			if (file.type.match(textType)) {
				var reader = new FileReader();

				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					//alert(fileDisplayArea);
					self.MapJSON(fileDisplayArea);
					self.map = JSON.parse(fileDisplayArea);
					//alert(JSON.stringify(self.map));
					self.drawMap(self.map);
				}

				reader.readAsText(file);	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	self.ImageInputId = ko.observable("image_background");
	
	self.loadImage = function(){
		var elem = document.getElementById(self.ImageInputId());
	   if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
	   }
		
		
		var fileInput = document.getElementById(self.ImageInputId());
		var fileDisplayArea = "";

		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var imageType = /image.*/;
			//alert(JSON.stringify(file.path));
			if (file.type.match(imageType)) {
				//var im = JSXMaster.board.create('image', [file.path, [-3,-2], [3,3]]);
				var reader = new FileReader();
				var img = new Image;
				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					img.src = reader.result;
					var w = img.width / JSXMaster.scale,
						h = img.height / JSXMaster.scale;
					var im = JSXMaster.board.create('image', [reader.result, [-3,-2], [w, h]]);
				}
				reader.readAsDataURL(file)	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	
	self.ZoomIn = function(){
		//console.log("Phong To");
		JSXMaster.board.zoomIn();
	}
	self.ZoomOut = function(){
		//console.log("Thu Nhon");
		JSXMaster.board.zoomOut();
	}
	self.Zoom100 = function(){
		//console.log("Normal");
		JSXMaster.board.zoom100();
	}
	self.Toolbar_Item_Hover = ko.observable("");
	self.ZoomBarId = ko.observable("zooming_bar_menu4");
	self.ZoomBarStatus = ko.observable("none");
	self.ShowZoomingBar = function(){
		if(self.ZoomBarStatus() === "none"){
			self.ZoomBarStatus("block");
			self.Toolbar_Item_Hover("toolbar_item_click");
		}else{ 
			self.ZoomBarStatus("none");
			self.Toolbar_Item_Hover("");
		}
		
	}
	self.OriginX = ko.observable(JSXMaster.board.origin.scrCoords[1]);
	self.OriginY = ko.observable(JSXMaster.board.origin.scrCoords[2]);
	self.ShiftLeft = function(){
		//alert(JSXMaster.board.origin.scrCoords[1] +" abc "+ JSXMaster.board.origin.scrCoords[2]);
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x - x*0.1, y);
	}
	self.ShiftRight = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x + x*0.1, y);
	}
	self.ShiftCenter = function(){
		//var x = JSXMaster.board.origin.scrCoords[1],
		//	y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(self.OriginX(), self.OriginY());
	}
	self.ShiftUp = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y + y * 0.1);
	}
	self.ShiftDown = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y - y * 0.1);
	}
};
var gui = new GUI();
gui.init();
ko.applyBindings(gui);